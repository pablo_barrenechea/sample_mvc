<?php

include ("core/Template.php");
include ("library/formater/Date.php");
include ("library/validator/Validator.php");
include ("library/validator/AlphaNumericValidator.php");
include ("library/validator/AgeValidator.php");
include ("library/validator/AmountCharsValidator.php");
include ("library/validator/BlankValidator.php");
include ("library/validator/EmailValidator.php");
include ("library/validator/BlacklistValidator.php");
include ("library/validator/DuplicateEmailValidator.php");
include ("library/validator/CaptchaValidator.php");
include ("library/validator/Field.php");
include ("library/validator/Form.php");

class Controller {

	const MIN_AGE =  14;	

	const MAX_AGE = 70;

	protected $global_template;

	protected $request;

	protected $local_template;

	protected $translations;

	public function __construct() {
		$this->request = $_REQUEST;
		$this->createTemplate();
		Translator::setLocale(GeoLocation::getCountry(), GeoLocation::getLang());
		$this->translations = Translator::getTranslations();
	}

	private function _translate() {
		foreach ($this->translations as $key => $value) {
			$this->global_template->set("tl_" . $key, $value);
		}
	}

	private function createTemplate() {
		$this->global_template = new Template();
	}

	protected function getCountries() {
		return R::getAll('select * from country');
	}

	protected function generateArrayYears() {
		$years = array();
		$current_year = date("Y");
		for ($i = self::MIN_AGE; $i < self::MAX_AGE; $i++) {
			array_push($years, $current_year - $i);
		}
		return $years;
	}

	protected function setMainContent($html) {
		$this->global_template->set("main", $html);
		$this->global_template->set("site_url", Config::SITE_URL);
		$this->_translate();
	}
	
	protected function getMainContent(){
		return $this->global_template->fetch('static/html/main.html');
	}
	
	protected function getPartialContent(){
		return $this->global_template->fetch('static/html/ajax.html');
	}

	public function response() {
		echo $this->global_template->fetch('static/html/main.html');
	}

	public function ajaxResponse() {
		echo $this->global_template->fetch('static/html/ajax.html');
	}

	public function jsonResponse($object_to_json) {
		echo json_encode($object_to_json);
	}

	protected function sendEmail($subject, $to, $to_name, $html ) {
		$mail = new PHPMailer();
		$mail->isSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 2;
		$mail->Host = Config::getEmailData("host");
		$mail->Port = Config::getEmailData("port");
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		$mail->Username = Config::getEmailData("username");
		$mail->Password = Config::getEmailData("password");
		$mail->setFrom(Config::getEmailData("from"), Config::getEmailData("from_name"));
		$mail->addAddress($to, $to_name);
		//Set the subject line
		$mail->Subject = $subject;
		$email_template = new Template();
		$mail->msgHTML($html, "");
		if (!$mail->send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Message sent!";
		}
	}

}
?>
