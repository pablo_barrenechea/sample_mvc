<?php

include ("controller/Controller.php");

class Country extends Controller {

	public function __construct() {
		parent::__construct();
	}
	
	public function getCities(){
		$country = R::findOne('country',' tld = ? ',array($this->request["country"]));
		$un = unserialize($country->cities);
		$response = array( "error" => 1, "cities" => $un );
		$this->jsonResponse($response);
	}
}

?>