<?php

include ("controller/Controller.php");

class Main extends Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	
	public function main(){
		$this->setMainContent("Latamis - questions");
		$this->response();
	}
}
