<?php

include ("controller/Controller.php");

class Register extends Controller {

	public function __construct() {
		parent::__construct();
	}

	const FIRST_STEP = "static/html/register/step1.html";
	const SECOND_STEP = "static/html/register/step2.html";
	const REGISTER_EMAIL = "static/html/register/register_email.html";
	const LAST_STEP = "static/html/register/last_step.html";
	const FB_LAST_STEP = "static/html/register/fb_last_step.html";
	
	
	public function firstStep() {
		$this->local_template = new Template();
		$local = $this->local_template->fetch(self::FIRST_STEP);
		$this->setMainContent($local);
		$this->response();
	}

	public function storeFirstStepData() {
		$form = new Form();
		
		$member = new Field($this->request["user_name"]);
		$member->addValidator(new BlankValidator( array("error_message" => $this->translations["Usuario_no_puede_ser_vacio"])));
		$form->addField($member);
		
		$email = new Field($this->request["email"]);
		$email->addValidator(new EmailValidator( array("error_message" => $this->translations["El_formato_del_email_no_es_valido"])));
		$email->addValidator(new DuplicateEmailValidator( array("error_message" => $this->translations["Ya_hay_un_usuario_registrado_con_ese_email"], "table_name" => "members", "field_name" => "email")));
		$form->addField($email);

		$password = new Field($this->request["password"]);
		$password->addValidator(new AlphaNumericValidator( array("error_message" => $this->translations["Password_debe_contener_al_menos_un_digito_y_un_caracter_alfabetico"])));
		$password->addValidator(new AmountCharsValidator( array("error_message" => $this->translations["Password_debe_contener_al_menos_6_caracteres"], "min_chars" => 6)));
		$form->addField($password);
		if ($form->validate()) {
			$member = R::dispense("members");
			$member->username = $this->request["user_name"];
			$member->email = $this->request["email"];
			$validator = new BlacklistValidator( array("error_message" => $this->translations["El_dominio_del_email_no_es_valido"], "table_name" => "domain_blacklist", "field_name" => "email"));
			$validator->validate($this->request["email"]) ? $member->blacklist = 0 : $member->blacklist = 1;
			$member->password = md5($this->request["password"]);
			$id = R::store($member);
			$this->jsonResponse(array( "error" => 0, "id" => $id ));
		} else {
			$this->jsonResponse(array( "error" => 1, "errors" => $form->getErrors() ));
		}

	}

	public function secondStep() {
		$this->local_template = new Template();
		$this->local_template->set("years", $this->generateArrayYears());
		$this->local_template->set("countries", $this->getCountries());
		$this->local_template->set("user_id", $this->request["member"]);
		$local = $this->local_template->fetch(self::SECOND_STEP);
		$this->setMainContent($local);
		$this->response();
	}

	public function storeSecondStepData(){
		$form = new Form();
		$gender_value = ( isset($this->request["gender"]) && $this->request["gender"] != "") ? $this->request["gender"] : "";
		$gender = new Field($gender_value);
		$gender->addValidator(new BlankValidator( array("error_message" => $this->translations["Sexo_no_puede_ser_vacio"])));
		$form->addField($gender);

		$day = new Field($this->request["day"]);
		$day->addValidator(new BlankValidator( array("error_message" => $this->translations["Dia_no_puede_ser_vacio"])));
		$form->addField($day);
		
		$month = new Field($this->request["month"]);
		$month->addValidator(new BlankValidator( array("error_message" => $this->translations["Mes_no_puede_ser_vacio"])));
		$form->addField($month);
		
		$year = new Field($this->request["year"]);
		$year->addValidator(new BlankValidator( array("error_message" => $this->translations["Anio_no_puede_ser_vacio"])));
		$form->addField($year);
		
		$country = new Field($this->request["country"]);
		$country->addValidator(new BlankValidator( array("error_message" => $this->translations["Pais_no_puede_ser_vacio"])));
		$form->addField($country);
		
		$city = new Field($this->request["city"]);
		$city->addValidator(new BlankValidator( array("error_message" => $this->translations["Ciudad_no_puede_ser_vacio"])));
		$form->addField($city);

		$captcha = new Field($this->request["captcha"]);
		$captcha->addValidator(new CaptchaValidator( array("error_message" => $this->translations["El_texto_ingresado_no_corresponde_con_la_imagen"])));
		$form->addField($captcha);		

		if ($form->validate()) {
			$member = R::load('members', $this->request["user_id"]);
			$member->gender = $this->request["gender"];
			$member->birth_date = DateFormater::getDateFromDefault($this->request["day"] ."/".$this->request["month"]."/".$this->request["year"]);
			$member->countryid = $this->request["country"];
			$member->city = $this->request["city"];
			$id = R::store($member);
			$this->jsonResponse(array( "error" => 0));
		} else {
			$this->jsonResponse(array( "error" => 1, "errors" => $form->getErrors() ));
		}
	}
	
	public function storeDeviceData(){
		$member = R::load('members', $this->request["user_id"]);
		$member->device = $this->request["device"];
		$id = R::store($member);
		$this->setMainContent("");
		$this->ajaxResponse();
	}

	public function lastStep(){
		$member = R::load('members', $this->request["user_id"]);
		$member->recive_survey = $this->request["receive"];
		$member->confirmation_code = $this->generateHashCode();
		$id = R::store($member);
	}

	private function generateHashCode(){
		return $random_hash = md5(uniqid(rand(), true));
	}	

	public function sendRegistrationEmail(){
		$member = R::load('members', $this->request["user_id"]);
		$this->local_template = new Template();
		$this->local_template->set("user_name", $member->username);
		$this->local_template->set("token_link",Config::SITE_URL."index.php?controller=register&action=activateAccount&token=" . $member->confirmation_code);
		$this->setMainContent($this->local_template->fetch(self::REGISTER_EMAIL));
		$this->sendEmail($this->translations["Latamis_register_email"], $member->email, "", $this->getPartialContent());
	}

	public function registerLanding(){
		$this->local_template = new Template();
		$this->local_template->set("user_id",$this->request["user_id"]);
		$this->setMainContent($this->local_template->fetch(self::LAST_STEP));
		$this->response();
	}


	public function activateAccount(){
		$member = R::findOne('members',' confirmation_code = ? ',array($this->request['token']));
		if( count($member) > 0 ){
			$member->confirmed = 1;
			$member->confirmation_date = date("Y-m-d H:i:s");
			$member->confirmation_code = "";
			R::store($member);
		}
		$this->local_template = new Template();
		$this->setMainContent($this->local_template->fetch(self::LAST_STEP));
		$this->response();
	}
	
	public function storeFBData(){
		$member = R::getRow("select * from members where email = '{$this->request["email"]}' and using_facebook = 1");
        if( count($member) > 0 ){
			$this->jsonResponse(array( "new_user" => 0, "user_id" => $member["id"]));
			return;
		}
		$member = R::dispense("members");
		$member->email = $this->request["email"];
		$date = explode("/", $this->request["birthday"]);
		if( count($date) == 3 ){
			$member->birth_date = DateFormater::getDateFromDefault($date[1] ."/".$date[0]."/".$date[2]);
		}
		if( $this->request["gender"] == "male" )
			$member->gender = 1;
		else
			$member->gender = 0;
		$member->registration_date = date("Y-m-d H:i:s");	
		$member->using_facebook = 1;
		$member->confirmed = 1;
		$member->blacklist = 0;
		R::store($member);
		$this->jsonResponse(array( "new_user" => 1, "user_id" => $member->id)); 
	}

	public function fbLastStep(){
		$this->local_template = new Template();
		$this->local_template->set("years", $this->generateArrayYears());
		$this->local_template->set("countries", $this->getCountries());
		$this->local_template->set("user_id", $this->request["user_id"]);
		$local = $this->local_template->fetch(self::FB_LAST_STEP);
		$this->setMainContent($local);
		$this->response();
	}
	
	public function storeCity(){
		$form = new Form();
		$country = new Field($this->request["country"]);
		$country->addValidator(new BlankValidator( array("error_message" => $this->translations["Pais_no_puede_ser_vacio"])));
		$form->addField($country);
		
		$city = new Field($this->request["city"]);
		$city->addValidator(new BlankValidator( array("error_message" => $this->translations["Ciudad_no_puede_ser_vacio"])));
		$form->addField($city);

		if ($form->validate()) {
			$member = R::load('members', $this->request["user_id"]);
			$member->countryid = $this->request["country"];
			$member->city = $this->request["city"];
			$id = R::store($member);
			$this->jsonResponse(array( "error" => 0));
		} else {
			$this->jsonResponse(array( "error" => 1, "errors" => $form->getErrors() ));
		}
	}
	
}
?>
