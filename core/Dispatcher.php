<?php

class Dispatcher {
    
    private $controller = "";
    private $action = "";
 
    public function __construct($controller, $action) {
    	$this->_initializeDB();	
    	$this->_initializeLocation();
        $this->action = $action;
        include("controller/" . ucfirst($controller) . ".php");
        $class_name = ucfirst($controller);
        if( class_exists($class_name) )
			$this->controller = new $class_name();
    }
    
    private function _initializeDB(){
        R::setup('mysql:host=' . Config::MYSQL_HOST . ';dbname=' . Config::MYSQL_DB ,Config::MYSQL_USER,Config::MYSQL_PASSWORD);
    }
    
	private function _initializeLocation(){
		GeoLocation::init();
	}
	
    public function dispatch(){
        $method_name = $this->action;
        $this->controller->$method_name();
    }
}

?>
