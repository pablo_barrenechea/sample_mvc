<?php

class Mysql {


    private $link_id;
    private $results;
    private $info;

    public function __construct() {
        $this->link_id = -1;
        $this->results = array();
        $this->info = array();
    }

    function connected() {
        return $this->link_id >= 0;
    }

    public function connect($host, $database, $user = "web", $password = "19851985") {
        //echo $host."::".$database."::".$user."::".$password;exit;
        if (!$this->connected()) {
            $this->link_id = mysql_connect($host, $user, $password);
            if (!$this->link_id)
                $this->error("connection failed");
            $tmp = mysql_select_db($database);
            if (!$tmp)
                $this->error("selectdb failed");
            $tmp = mysql_query("SET NAMES utf8", $this->link_id);
            if (!$tmp)
                $this->error("No set UTF8");
            mysql_query("SET SESSION sql_mode = ''", $this->link_id); //Compatibilidad cadenas vacias en enteros
        }
    }

    private function free_result($r) {
        if (!$this->connected())
            $this->error("!!(5)");
        if ($this->results[$r]) {
            $this->results[$r] = false;
            mysql_free_result($r);
            return;
        } else {
            $this->error("!!(6)");
        }
    }

    public function close() {
        if ($this->connected()) {
            mysql_close($this->link_id);
            $this->link_id = -1;
        }
    }

    public function query($q, $file = "__FILE__", $line = "__LINE__") {
        if (!$this->connected())
            $this->error("must connect first");

        $tmp = mysql_query($q, $this->link_id);
        if (!$tmp) {
            $mensaje = "<HR>";
            $mensaje .= "<BR>MYSQL-ERROR : " . mysql_error($this->link_id);
            $mensaje .= "<BR>FILE : $file : $line";
            $mensaje .= "<PRE>$q</PRE>";
            $mensaje .= "<HR>";
            echo $mensaje;
            $this->close();
        }

        $this->results[$tmp] = true;
        $this->info[$tmp] = array("file" => $file, "line" => $line, "q" => $q);

        return $tmp;
    }

    public function query_array($q, $file = "__FILE__", $line = "__LINE__") {
        $array = array();
        $tmp = $this->query($q, $file, $line);
        $n = $this->num_rows($tmp);

        for ($i = 0; $i < $n; $i++)
            $array[$i] = $this->fetch_array($tmp, $i);
        $this->free_result($tmp);
        return $array;
    }

    public function fetch_array($r, $row) {
        if (!$this->connected())
            $this->error("!!(7)");

        if ($this->results[$r])
            return mysql_fetch_array($r, MYSQL_ASSOC);
        else
            $this->error("!!(4)");
    }

    public function fetch_row($r, $row) {
        if ($this->results[$r])
            return mysql_fetch_row($r, $row);
        else
            $this->error("!!(3)");
    }

    public function num_rows($r) {
        if ($this->results[$r])
            return mysql_num_rows($r);
        else
            $this->error("!!(1)");
    }

    public function result($t, $r, $c) {
        if ($this->results[$t])
            return mysql_result($t, $r, $c);
        else
            $this->error("!!(2)");
    }

    public function error($msg) {

        echo ("ERROR : $msg");
        $this->close();
    }

    public function last_id() {
        return mysql_insert_id($this->link_id);
    }

}

?>
