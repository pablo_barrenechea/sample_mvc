<?php 
	session_start();
    include("core/Config.php");
    include("library/Readbean.php");
	include("library/geo/GeoLocation.php");
    include("library/translate/Translator.php");
	include("library/session/SessionHandler.php");
    include("library/mail/PHPMailerAutoload.php");
    include("core/dispatcher.php");
    $dispatcher = new Dispatcher($_REQUEST['controller'],$_REQUEST["action"]);
    $dispatcher->dispatch();
?>