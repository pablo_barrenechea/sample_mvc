<?php

class DateFormater{
	
    public static function getDateFromDefault($date_default,$format="Y-m-d H:i:s"){
        if($date_default != null){
            $date = explode("/",$date_default);
            $year = $date[2];
            $month = $date[1];
            $day = $date[0];
            $timestamp = mktime(0,0,0,$month,$day,$year);
            return date($format, $timestamp);
        }
        return "";
    }
		
}

?>