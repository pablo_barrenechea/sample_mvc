<?php

class GeoLocation{
	
	static private $country;
	
	static private $lang;
	
	public static function init(){
		//Hardcoding ip	
		if( SessionCookieHandler::getProperty("country") && SessionCookieHandler::getProperty("lang") ){
			self::$country = SessionCookieHandler::getProperty("country");
			self::$lang = SessionCookieHandler::getProperty("lang");		
		}else{
			$_SERVER['REMOTE_ADDR'] = "69.197.132.80";
			self::$country = strtoupper(file_get_contents("http://geoip.wtanaka.com/cc/{$_SERVER['REMOTE_ADDR']}"));
			SessionCookieHandler::setProperty("country", self::$country);
			$country = R::findOne('country',' tld = ? ',array(strtolower(self::$country)));
			self::$lang = $country->lang;
			SessionCookieHandler::setProperty("lang", self::$lang);
		}

	}
	
	static public function getCountry(){
		return self::$country;
	}
	
	static public function getLang(){
		return self::$lang;
	}
}

?>