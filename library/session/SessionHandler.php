<?php

class SessionCookieHandler {
	
	const SESSION_NAMESPACE = Config::SESSION_NAMESPACE;
	
	public static function setProperty($key, $value){
		if( !isset($_SESSION[self::SESSION_NAMESPACE])  )
			$_SESSION[self::SESSION_NAMESPACE] = array(); 
		$_SESSION[self::SESSION_NAMESPACE][$key] = $value;
	}
	
	public static function getProperty($key){
		return ( isset($_SESSION[self::SESSION_NAMESPACE][$key]) && !empty($_SESSION[self::SESSION_NAMESPACE][$key]) ? $_SESSION[self::SESSION_NAMESPACE][$key] : NULL );
	}
}

?>
