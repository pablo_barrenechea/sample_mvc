<?php

include ("library/translate/POHandler.php");

class Translator {

	static private $po_hanlder_instance = null;

	static private $locale = "en_US";

	const DEFAULT_LANG = "US";

	public static function setLocale($country, $lang) {
		if( self::DEFAULT_LANG != $country ){
			self::$locale = strtolower($lang) . "_" . strtoupper($lang);
			self::$po_hanlder_instance = new POHandler(self::$locale);	
		}else{
			self::$po_hanlder_instance = new POHandler(self::$locale);
		}
	}

	public static function getTranslations() {
		return self::$po_hanlder_instance -> getTranslations();
	}

	public static function translateText($key) {
		return self::$po_hanlder_instance -> getText($key);
	}

	public static function getLocale() {
		return self::$locale;
	}

	public static function getPOHandlerInstance() {
		return self::$po_hanlder_instance;
	}

}
?>
