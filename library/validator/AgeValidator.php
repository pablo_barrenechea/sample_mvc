<?php

class AgeValidator extends Validator{
	
	private $min_age;
	
	public function __construct($config){
		parent::__construct($config);
		$this->min_age = (isset( $this->config["age"]) && !empty($this->config["age"]) ) ? $this->config["age"] : -1;
	}
	
	protected function checkValue($value){
		$value = explode("-", $value);
		$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md") ? ((date("Y")-$birthDate[0])-1):(date("Y")-$birthDate[0]));
		if( $age >= $this->min_age )
			return true;
		return false;
	}
}

?>