<?php

class AlphaNumericValidator extends Validator {
	
	public function __construct($config){
		parent::__construct($config);
	}
	
	protected function checkValue($value){
		if (preg_match('/([a-z|A-Z])+/', $value) && preg_match('/([0-9])+/', $value) )
			return true;
		return false;
	}
	
}

?>