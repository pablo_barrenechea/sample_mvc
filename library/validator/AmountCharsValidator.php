<?php

class AmountCharsValidator extends  Validator{
	
	private $min;
	
	private $max;
	
	public function __construct($config){
		parent::__construct($config);
		$this->min = (isset( $this->config["min_chars"]) && !empty($this->config["min_chars"]) ) ? $this->config["min_chars"] : -1;
		$this->max = (isset( $this->config["max_chars"]) && !empty($this->config["max_chars"]) ) ? $this->config["max_chars"] : -1;
	}
	
	protected function checkValue($value){
		$length = strlen($value);
		if( $this->min == -1 || $this->min <= $length  ){
			if( $this->max == -1 || $this->min >= $length )
				return true;
			else {
				return false;
			}
		}else{
			return false;
		} 
	}
}

?>