<?php

class BlankValidator extends Validator{
	
	public function __construct($config){
		parent::__construct($config);
	}
	
	protected function checkValue($value){
		return !(empty($value) || $value == "");
	}
}

?>