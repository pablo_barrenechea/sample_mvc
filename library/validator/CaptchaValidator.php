<?php

include("library/securimage/securimage.php");

class CaptchaValidator extends Validator{
	
	public function __construct($config){
		parent::__construct($config); 
	}
	
	protected function checkValue($value){
		$img  = new Securimage();	
		if ($img->getCode() == $value) {
    		return true;
     	} else {
     	  return false;
      	}
	}
}

?>