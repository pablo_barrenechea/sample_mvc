<?php

class DuplicateEmailValidator extends Validator{
	
	private $table_name;
	
	private $field_name;
	
	public function __construct($config){
		parent::__construct($config);
		$this->table_name = (isset( $this->config["table_name"]) && !empty($this->config["table_name"]) ) ? $this->config["table_name"] : "";
		$this->field_name = (isset( $this->config["field_name"]) && !empty($this->config["field_name"]) ) ? $this->config["field_name"] : "";
		
	}
	
	protected function checkValue($value){
		if( $this->table_name != "" && $value != ""){
			$emails = R::find($this->table_name," {$this->field_name} = ? ", array( $value ) );
			if( count($emails) > 0 )
				return false;
			else
				return true;
		}else{
			return true;
		}
			
	}
}

?>