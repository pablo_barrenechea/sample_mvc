<?php

class EmailValidator extends Validator{
	
	public function __construct($config){
		parent::__construct($config);
	}
	
	protected function checkValue($value){
		return filter_var($value, FILTER_VALIDATE_EMAIL);
	}
}

?>