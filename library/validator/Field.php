<?php
	class Field{
		
		private $value;
		private $errors;
		private $validators;
		private $is_valid;
		
		public function __construct($value){
			$this->value = $value;	
			$this->validators = array();
			$this->errors = array();
			$this->is_valid = true;
		}
		
		public function addValidator( Validator $v ){
			array_push($this->validators,$v);
		}
		
		public function getErrors(){
			return $this->errors;
		}
		
		public function isValid(){
			return $this->is_valid;
		}
		
		public function validate(){
			foreach( $this->validators as $v ){
				if( !$v->validate($this->value)){
					$this->is_valid = false;
					array_push($this->errors, $v->getError());
				}
			}
		}
	}
?>