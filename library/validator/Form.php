<?php

class Form {
	
	private $fields;
	private $errors;
	
	
	public function __construct(Array $fields = array()){
		$this->fields = $fields;
		$this->errors = array();	
	}
	
	public function addField($field){
		array_push($this->fields, $field);
	}
	
	public function getErrors(){
		return $this->errors;
	}
	
	public function validate(){
		$valid = true;
		foreach( $this->fields as $f ){
			$f->validate();
			if( !$f->isValid() ){
				$this->errors = array_merge($this->errors,$f->getErrors());
				$valid = false;
			}
		}
		return $valid;
	}
}

?>