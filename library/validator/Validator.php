<?php

abstract class Validator{
	
	protected $config;
	
	protected $error;
	
	public function __construct($config = array()){
		$this->config = $config;
	}
	
	protected abstract function checkValue($value);
	
	public function validate($value){
		if( $this->checkValue($value) )
			return true;
		try{
			$this->error = $this->config["error_message"];
			return false;	
		}catch(exception $e){
			echo "Error message is missing";
		} 
	}
	
	public function getError(){
		return $this->error;
	}
}

?>

